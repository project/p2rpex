<?php

/**
 * @file
 * Contains profile2_regpath_exportable.
 */

/**
 * Implements hook_features_export_options().
 */
function profile2_regpath_exportable_features_export_options() {
  $options = array();
  _profile2_regpath_exportable_get_options($options);
  return $options;
}

/**
 * Implements hook_features_export().
 */
function profile2_regpath_exportable_features_export($data, &$export, $module_name) {
  $export['dependencies']['profile2_regpath_exportable'] = 'profile2_regpath_exportable';
  foreach ($data as $component) {
    $export['features']['profile2_regpath_exportable'][$component] = $component;
  }
}

/**
 * Implements hook_features_export_render().
 */
function profile2_regpath_exportable_features_export_render($module_name, $data, $export = NULL) {
  $code = array();
  foreach ($data as $options) {
    $comp_option = explode(':', $options);
    $code[$options] = _profile2_regpath_exportable_get_regpath_settings($comp_option[1], $comp_option[0]);
  }
  $code = " return " . features_var_export($code, ' ') . ';';
  return ['profile2_regpath_exportable_default_settings' => $code];
}

/**
 * Loads all existing profiles making them available for exporting in features.
 *
 * @param array $options
 *    Options array.
 */
function _profile2_regpath_exportable_get_options(&$options) {
  // Get profile information.
  $q = db_select('profile_type', 'pt');
  $results = $q->fields('pt')->execute();
  foreach ($results as $record) {
    $options[$record->type . ':' . $record->id] = $record->label;
  }
}

/**
 * Helper function, to load Regpath values from db.
 *
 * @param int $id
 *    Profile id.
 * @param string $type
 *    Profile type.
 */
function _profile2_regpath_exportable_get_regpath_settings($id, $type) {
  $q = db_select('profile2_regpath', 'rg');
  $q->condition('rg.profile_id', $id)
    ->fields('rg');
  $result = $q->execute()->fetchAssoc();
  // Due to logic in regpath, we need to store the type value,
  // we can later match to the profile_type table instead of using profile_id,
  // which was possible incremented.
  $profile_type = variable_get('profile2_regpath_exportable_' . $id);
  if (empty($profile_type)) {
    variable_set('profile2_regpath_exportable_' . $id, $type);
  }
  return $result;
}

/**
 * Implements hook_features_rebuild().
 *
 * When features is rebuilding we will update Regpath values with what we have
 * in code.
 */
function profile2_regpath_exportable_features_rebuild($module_name) {
  $defaults = features_get_default('profile2_regpath_exportable', $module_name);
  if (!empty($defaults)) {
    foreach ($defaults as $regpath_values) {
      $profile_id = $regpath_values['profile_id'];
      $fields['path'] = $regpath_values['path'];
      $fields['roles'] = $regpath_values['roles'];
      $fields['status'] = $regpath_values['status'];
      $fields['weight'] = $regpath_values['weight'];
      $fields['misc'] = $regpath_values['misc'];
      // Update regpath values.
      if (_profile2_regpath_check_profile_id($profile_id)) {
        db_merge('profile2_regpath')
          ->key(['profile_id' => $profile_id])
          ->fields($fields)
          ->execute();
      }
      else {
        // Couldn't find a match on id, we will use type and update id key.
        $profile_type = variable_get('profile2_regpath_exportable_' . $profile_id);
        $q = db_select('profile_type', 'pt');
        $q->condition('pt.type', $profile_type)
          ->fields('pt', ['id']);
        $profile_type_id = $q->execute()->fetchCol();
        if ($profile_type_id) {
          // Update key.
          db_update('profile2_regpath')
            ->fields(['profile_id' => $profile_type_id[0]])
            ->condition('profile_id', $profile_id)
            ->execute();
          db_merge('profile2_regpath')
            ->key(['profile_id' => $profile_type_id[0]])
            ->fields($fields)
            ->execute();
        }
      }
    }
  }
}

/**
 * Implements hook_features_revert().
 */
function profile2_regpath_exportable_features_revert($module_name) {
  profile2_regpath_exportable_features_rebuild($module_name);
}

/**
 * Checks if id is still the same one.
 *
 * @param int $id
 *    Id number of profile.
 *
 * @return bool
 *    True if $id was found and False if not.
 */
function _profile2_regpath_check_profile_id($id) {
  $q = db_select('profile_type', 'pt');
  $q->condition('pt.id', $id)
    ->fields('pt');
  $result = $q->execute()->fetchAssoc();
  if (!empty($result)) {
    return TRUE;
  }
  return FALSE;
}
