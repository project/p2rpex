
-- SUMMARY --

The profile2 regpath exportable module enables you to export your regpath
settings in features.

-- REQUIREMENTS --

Features
https://www.drupal.org/project/features

Profile2
https://www.drupal.org/project/profile2

Profile2 Registration Path
https://www.drupal.org/project/profile2_regpath

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- Configuration --
None

-- FAQ --

Q: Why my feature appears to be always overwritten, in Profile2 Regpath
exportables?

A: If your features includes Profile2 and if you reverted it, there is a chance
that the profile_id and id don't match, you will have to update your feature,
by either re-creating it using features UI or using drush fu.

-- CONTACT --

Current maintainers:
jorge Alves - https://www.drupal.org/u/jorgealves
